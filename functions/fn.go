package functions

import (
	"fmt"
	"net/http"

	"gitlab.com/terraform-test2/server/common/model"
)

// HelloWorld prints "Hello, world."
func HelloWorld(w http.ResponseWriter, r *http.Request) {
	var a model.Car
	a.Name = "206"
	fmt.Fprintf(w, "Hello world my little %s", a.Name)
}
